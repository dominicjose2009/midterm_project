#include <iostream>
#include <conio.h>
#include <list>

using namespace std;

class Node
{
public:
		string	data;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* InsertNode(int index,string x);
			int FindNode(string x);
			int DeleteNode(int index);
			void DisplayList(void);
			void DisplayPlaylist(void);
			string ViewMusic(int index);
			int Size(Node* index);
	private:
			Node* head;
};
int List::FindNode(string x){
	Node* CurrNode	=	head;
	int	CurrIndex	=	0;
	while (CurrNode && CurrNode->data != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: DeleteNode(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	int CurrIndex	=	7;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode){
			prevNode->next	=	CurrNode->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			delete	CurrNode;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->data;
	return NULL;
}
void List::DisplayPlaylist(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	while (CurrNode != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->data<<endl;
		CurrNode	=	CurrNode->next;
		num++;
	}
	
}
Node* List::InsertNode(int index,string x){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	Node* newNode =		new		Node;
	newNode->data = 	x;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
	}
	return newNode;
}
List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}

int List::Size(Node* index){
    int count = 0; // Initialize count  
    Node* current = head; // Initialize current  
    while (current != NULL)  
    {  
        count++;  
        current = current->next;  
    }  
    return count;  
}  

int main(){
	string mus,num;
	List MusicList;
	MusicList.InsertNode(0,"Hikaru Nara - Goose House");
	MusicList.InsertNode(1,"7-Colored Symphony - Coalamade");
	MusicList.InsertNode(2,"Orange - 7!!");
	MusicList.InsertNode(3,"Kirameki - Wacci");
	again:
	cout<<"Welcome to your Music Hub!\nPlease type 1 to Add a new Music title\nPlease type 2 to View/Play a Music title\nPlease Type 3 to Edit a Music title\nPlease Type 4 to Delete a Music title\nPlease Type 5 to view Playlist\nPlease Type 6 to Exit\nEnter the chosen Number : ";
	cin>>num;
	if (num=="1"){
		string mus;
		cout<<"Please Enter the Music Title : "<<endl;
		cin.ignore();
		getline(cin,mus);
		MusicList.InsertNode(0,mus);
		goto again;
	}
	else if(num=="2"){
		int musnum;
		Node* CurrNode;
		cout<<"Choose which number of Music you want to play : "<<endl;
		MusicList.DisplayList();
		cin>>musnum;
		if(musnum-1 <= 0){
			cout<<"No Song Available.\n";
		}
		else{
			cout<<"Previous : "<<MusicList.ViewMusic(musnum-1)<<endl;
		}
		if(musnum > MusicList.Size(CurrNode)){
			cout<<"No Song Available.\n";
		}
		else{
		cout<<"Now Playing : "<<MusicList.ViewMusic(musnum)<<endl;
		}
		if(musnum+1 >= MusicList.Size(CurrNode)){
			cout<<"No Song Available.\n";
		}
		else{
			cout<<"Next : "<<MusicList.ViewMusic(musnum+1)<<endl;
		}
	}
	else if(num=="3"){
	int editnum;
	string editname;
	cout<<"Choose which number of the Music you want to edit : "<<endl;
	MusicList.DisplayList();
	cin>>editnum;
	
	MusicList.DeleteNode(editnum);
	cout<<"Enter the new Music title : ";
	cin.ignore();
	getline(cin,editname);
	cout<<endl;
	MusicList.InsertNode(editnum-1,editname);
	goto again;
	}
	else if(num=="4"){
	int del;
	cout<<"Choose which number of the Music you want to delete : "<<endl;
	MusicList.DisplayList();
	cin>>del;
	MusicList.DeleteNode(del);
	goto again;
	}
	else if(num=="5"){
		MusicList.DisplayPlaylist();
		cout<<endl;
		goto again;
	}
	else if(num=="6"){
		return 0;
	}
	else{
		cout<<"Enter a number\nPlease Try Again\n"<<endl;
		goto again;
	}
}